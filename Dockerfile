from grafana/grafana

USER root

# installing plugins
RUN apk add --no-cache curl unzip && \
  curl -Lo node_exporter.json https://grafana.com/api/dashboards/1860/revisions/21/download && \
  mkdir -p /opt/cactus/grafana/dashboards && mv node_exporter.json /opt/cactus/grafana/dashboards && \
  curl -Lo aceiot.zip https://grafana.com/api/plugins/aceiot-svg-panel/versions/0.0.8/download && \
  unzip aceiot.zip -d plugins > /dev/null && \
  mkdir /var/lib/grafana/plugins/aceiot-svg-panel && \
	mv plugins/ACE-IoT-Solutions-ace-svg-react-* /var/lib/grafana/plugins/aceiot-svg-panel && \
	rm -rf aceiot.zip && \
  curl -Lo discrete.zip https://grafana.com/api/plugins/natel-discrete-panel/versions/0.1.1/download && \
  unzip discrete.zip -d plugins > /dev/null && \
  mkdir /var/lib/grafana/plugins/natel-discrete-panel && \
	mv plugins/natel-discrete-panel* /var/lib/grafana/plugins/natel-discrete-panel/ && \
	rm -rf discrete.zip

# copying confs

COPY conf /etc/grafana

USER grafana

ENV GF_AUTH_ANONYMOUS_ENABLED="true" \
    GF_AUTH_ORG_ROLE="viewer" \
    GF_AUTH_DISABLE_LOGIN_FORM="true"

