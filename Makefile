SHELL:=/bin/bash
.DEFAULT_GOAL := rpm

#
# main variables
#

VERSION ?= $(shell git describe --always)
GRAFANA_VERSION = 11.5.2
RELEASE = 1
ARCH = amd64
RPM_NAME = cactus-grafana-${VERSION}-${RELEASE}.${ARCH}.rpm

#
# main targets
#

.PHONY: rpm
rpm: ${RPM_NAME}

.PHONY: clean
clean:
	rm -rf rpmroot rpms

#
# dependencies
#

GRAFANA_FOLDER = grafana-${GRAFANA_VERSION}
${GRAFANA_FOLDER}:
	curl -OL https://dl.grafana.com/oss/release/${GRAFANA_FOLDER}.linux-${ARCH}.tar.gz
	tar -xzvf ${GRAFANA_FOLDER}.linux-${ARCH}.tar.gz
	[ -d grafana-v${GRAFANA_VERSION} ] && mv grafana-v${GRAFANA_VERSION} grafana-${GRAFANA_VERSION}
	rm -f ${GRAFANA_FOLDER}.linux-${ARCH}.tar.gz
	touch ${GRAFANA_FOLDER}

.PHONY: externals
externals: dashboards/software/node_exporter.json dashboards/software/blackbox_exporter.json \
	plugins/aceiot-svg-panel plugins/grafana-piechart-panel \
	plugins/jdbranham-diagram-panel \

dashboards/software/node_exporter.json:
	mkdir -p dashboards/software
	curl -Lo dashboards/software/node_exporter.json https://grafana.com/api/dashboards/1860/revisions/21/download

dashboards/software/blackbox_exporter.json:
	mkdir -p dashboards/software
	curl -Lo dashboards/software/blackbox_exporter.json https://grafana.com/api/dashboards/7587/revisions/3/download

plugins/aceiot-svg-panel:
	curl -Lo aceiot.zip https://grafana.com/api/plugins/aceiot-svg-panel/versions/0.0.8/download
	unzip aceiot.zip -d plugins > /dev/null
	mv plugins/ACE-IoT-Solutions-ace-svg-react-* plugins/aceiot-svg-panel
	rm -rf aceiot.zip

plugins/grafana-piechart-panel:
	curl -Lo piechart.zip https://grafana.com/api/plugins/grafana-piechart-panel/versions/1.6.1/download
	unzip piechart.zip -d plugins > /dev/null
	rm -rf piechart.zip

plugins/jdbranham-diagram-panel:
	curl -Lo diagram.zip https://grafana.com/api/plugins/jdbranham-diagram-panel/versions/1.7.3/download	
	unzip diagram.zip -d plugins > /dev/null
	rm -rf diagram.zip

${RPM_NAME}: ${GRAFANA_FOLDER} externals
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus rpmroot/opt/cactus/bin/grafana/ \
		rpmroot/usr/lib/systemd/system rpmroot/opt/cactus/var/lib/grafana \
		rpmroot/opt/cactus/etc/default rpmroot/opt/cactus/etc/grafana \
		rpmroot/opt/cactus/etc/grafana/conf/provisioning/plugins

	cp systemd/* rpmroot/usr/lib/systemd/system
	cp -r ${GRAFANA_FOLDER}/bin/* rpmroot/opt/cactus/bin/grafana/
	cp -rf conf dashboards rpmroot/opt/cactus/etc/grafana
	cp -r plugins rpmroot/opt/cactus/var/lib/grafana
	cp -r ${GRAFANA_FOLDER}/public rpmroot/opt/cactus/var/lib/grafana/
	cp default/cactus-grafana.default rpmroot/opt/cactus/etc/default/cactus-grafana

	mkdir -p rpms
	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-grafana \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--iteration ${RELEASE} \
	--vendor CERN \
	--description "grafana for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/grafana" \
	--provides cactus-grafana \
	.=/ && mv *.rpm ../rpms/
