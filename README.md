# How to develop grafana in your own laptop

## Install grafana	

- Install grafana in your own laptop: https://grafana.com/docs/grafana/latest/setup-grafana/installation/
- Test it works by visiting http://localhost:3000
		
## Tunnel to l1ts-prometheus.cms behind the P5 firewall (you need to have access to the node)
- Add these lines to your .ssh/config (change carrillo for your user)
```
		Host cmsusr.cern.ch
	  	User carrillo
		ProxyJump lxplus.cern.ch

		Host l1ts-prometheus.cms
		User carrillo
		ProxyJump cmsusr.cern.ch
```
-  Then tunnel via ssh the trafic from port 9090 to your laptop: ``` ssh -L 9090:localhost:9090 l1ts-prometheus.cms```

## Change the grafana datasource
Configuration -> Data Sources -> URL (type http://l1ts-prometheus.cms:9090)

## Import your favorite dashboards
- All L1 dashboards are public in gitlab https://gitlab.cern.ch/cms-cactus/ops/monitoring/grafana/-/tree/master/dashboards
- You can download and then import them in your grafana: Dashboards -> Import -> Upload JSON file
- Live information from P5 will be displayed in your screen.
	
## You are set to start development
- Visit https://grafana.com/docs/grafana/next/getting-started/build-first-dashboard/?pg=oss-graf&plcmt=resources
	 
	
		
